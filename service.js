'use strict';

const logger = new (require('./logger'))();

class Service {
  doSomething(value) {
    logger.log(`Starting Service.doSomething(${value})...`);

    return new Promise((resolve, reject) => {
      if(value === undefined) {
        return reject('\'value\' is not specified');
      }
      setTimeout(() => {
        logger.log(`Finished Service.doSomething(${value})`);
        resolve(value)
      }, 150);
    })
  }
}

const instance = new Service();

module.exports = instance;
