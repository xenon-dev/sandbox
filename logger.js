'use strict';

const sandbox = require('continuation-local-storage').getNamespace('TestAppStorage');
// const sandbox = require('./sandbox');


class Logger {
  log(message) {
    const id = sandbox.get('id');
    console.log(`  [#${id}] - ${message}`);
  }

  error(message) {
    const id = sandbox.get('id');
    console.error(`  [#${id}] - ${message}`);
  }
}

module.exports = Logger;
