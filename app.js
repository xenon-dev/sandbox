'use strict';

const sandbox = require('continuation-local-storage').createNamespace('TestAppStorage');
// const sandbox = require('./sandbox');
const logger = new (require('./logger'))();
const service = require('./service');

start();


function start() {
  const PROCESS_COUNT = 3;
  let processes = new Array(PROCESS_COUNT);
  for(let nProcess = 1; nProcess <= PROCESS_COUNT; ++nProcess) {
        processes.push(startProcess(nProcess));
  }
  Promise.all(processes)
      .then(() => {
        console.log('App finished ============================')
      });

  console.log(`App started. Processes: ${PROCESS_COUNT} ================`);
}

function startProcess(processId) {
  return new Promise((resolve, reject) => {
    process.nextTick(() => {
      sandbox.run(session => {
        session.id = processId;

        myFunc()
            .then(result => resolve(result))
            .catch(err => reject(err));
      });
    });
  });
}

function myFunc() {
  const value = rand(1, 100);
  return service.doSomething(value % 2 ? undefined : value)
      .then(result => logger.log(`Service executed with result ${result}`))
      .catch(err => logger.error(`Service executed with error: ${err}`));
}

function rand(min, max) {
  return Math.floor(Math.random() * (max - min)) + min;
}
